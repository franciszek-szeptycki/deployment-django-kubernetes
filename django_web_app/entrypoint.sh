#!/bin/sh

# run migrations after the database is up
while ! nc -z db 5432; do
  echo "Waiting for the database..."
  sleep 1
done

python manage.py migrate

python manage.py shell <<EOF
from django.contrib.auth.models import User

if not User.objects.filter(username='admin').exists():
    User.objects.create_superuser('admin', 'admin@example.com', 'admin')
EOF


gunicorn django_web_app.wsgi:application -b 0.0.0.0:8000 
